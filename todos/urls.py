from django.urls import path
from todos.views import todo_list_list, show_todo_list, create_todolist, edit_todoList, delete_Todolist, create_todoListItem,todolist_update_item


urlpatterns = [
    path("", todo_list_list, name="todo_list_list"),
    path("<int:id>/", show_todo_list, name="show_todo_list"),
    path("create/", create_todolist, name="create_todo_list"),
    path("<int:id>/edit/", edit_todoList, name="edit_todoList"),
    path("<int:id>/delete/", delete_Todolist, name="delete_Todolist"),
    path("items/create/", create_todoListItem, name="create_todo_list_item"),
    path("items/<int:id>/edit/",todolist_update_item,name="todolist_update_item")
]
