from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList,TodoItem
from todos.forms import TodoListForm,TodoItemForm
# Create your views here.


def todo_list_list(request):
    to_do_list = TodoList.objects.all()
    context = {
        "to_do_list": to_do_list
    }
    return render(request, "todos/list.html", context)


def show_todo_list(request, id):
    todo_list_list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_list": todo_list_list
    }
    return render(request, "todos/detail.html", context)


def create_todolist(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }

    return render(request, "todos/create.html", context)


def edit_todoList(request, id):
    post = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=post)

        if form.is_valid():
            form.save()

            return redirect("show_todo_list", id=id)
    else:
        form = TodoListForm(instance=post)

    context = {
        "post_object": post,
        "post_form": form,
    }

    return render(request, "todos/edit.html", context)


def delete_Todolist(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("to_do_list_list")
    return render(request, "todos/delete.html")


def create_todoListItem(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")
    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }

    return render(request, "todos/newTask.html", context)


def todolist_update_item(request,id):
    post = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=post)

        if form.is_valid():
            item = form.save()
            return redirect("show_todo_list", id=item.list.id)
    else:
        form = TodoItemForm(instance=post)

    context = {
        "post_object": post,
        "post_form": form,
    }

    return render(request, "todos/edit.html", context)
